///<reference path="../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import {Component, Injectable, NgModule, OnInit} from '@angular/core';

import {AuthResponse, FacebookService, InitParams, LoginOptions, LoginResponse, LoginStatus} from 'ngx-facebook';
import {FacebookSocialModule} from '../Service/facebook-social.module';
import {Router} from '@angular/router';

const options: LoginOptions = {
  scope: 'public_profile,user_friends,email',
  return_scopes: true,
  enable_profile_selector: true
};

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})

export class UserLoginComponent implements OnInit {

 data: any;
  private uid: string;
  private accessToken: string;
  status: string;

  constructor(private fb: FacebookSocialModule, private router: Router) {
  }
  getStatus(): any  {
    this.fb.getStatus()
      .then(
        (res: LoginStatus) => {
          if (res.status === 'connected') {
            this.uid = res.authResponse.userID;
            this.accessToken = res.authResponse.accessToken;
            this.router.navigate(['list']);
          }else {
            console.log('you are not connected');
            console.log(res.status);
          }
        }
      );
  }
  loginFB() {
    this.fb.loginWithOptions();
    this.getStatus();
  }
  logout() {
    this.fb.signOut();
  }
  getFriends() {
    this.fb.getFriends();
  }
  ngOnInit() {
    this.fb.getStatus();
  }
}
