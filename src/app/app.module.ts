import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Route, RouterModule} from '@angular/router';
import { ListViewComponent } from './list-view/list-view.component';
import { ItemViewComponent } from './item-view/item-view.component';
import {HttpClientModule} from '@angular/common/http';
import { UserLoginComponent } from './user-login/user-login.component';
import {FacebookModule} from 'ngx-facebook';
import {FacebookSocialModule} from './Service/facebook-social.module';
import {HeaderComponent} from './header/header.component';
import { AddComponent } from './add/add.component';
import { LandingPageComponent } from './landing-page/landing-page.component';

const APP_ROUTES: Route[] = [
  {
    path: 'list',
    component: ListViewComponent
  },
  {
    path: 'item',
    component: ItemViewComponent
  },
  {
    path: 'edit',
    component: ItemViewComponent
  },
  {
    path: '',
    component: LandingPageComponent
  }
];
@NgModule({
  declarations: [
    AppComponent,
    ListViewComponent,
    ItemViewComponent,
    UserLoginComponent,
    HeaderComponent,
    AddComponent,
    LandingPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(APP_ROUTES),
    FacebookModule.forRoot(),
  ],
  providers: [AppComponent, FacebookSocialModule],
  bootstrap: [AppComponent]
})
export class AppModule {
}
