import {ItemViewComponent} from './item-view/item-view.component';
import {ListViewComponent} from './list-view/list-view.component';
import {UserLoginComponent} from './user-login/user-login.component';
import {Route} from '@angular/router';

export const APP_ROUTES: Route[] = [
  {
    path: 'list',
    component: ListViewComponent
  },
  {
    path: 'item',
    component: ItemViewComponent
  },
  {
    path: 'login',
    component: UserLoginComponent
  }
] ;
