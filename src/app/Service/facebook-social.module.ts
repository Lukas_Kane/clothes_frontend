import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthResponse, FacebookService, InitParams, LoginOptions, LoginResponse, LoginStatus} from 'ngx-facebook';
import {Router} from '@angular/router';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class FacebookSocialModule {
  authResponse: AuthResponse = this.fb.getAuthResponse();
  data: any;
  private uid: string;
  private accessToken: string;
  status: string;
  private loginStatus: Promise<LoginStatus>;

  constructor(private fb: FacebookService, private router: Router) {

    const initParams: InitParams = {
      appId: '335429586978253',
      xfbml: true,
      cookie: true,
      status: true,
      version: 'v2.12'
    };

    console.log('Initializing Facebook');
    fb.init(initParams);
    console.log(initParams.version);

  }
  loginWithOptions(): any {
    const loginOptions: LoginOptions = {
      enable_profile_selector: true,
      return_scopes: true,
      scope: 'public_profile,user_friends,email, user_birthday'
    };

    this.fb.login(loginOptions)
      .then((res: LoginResponse) => {
        if (res.status === 'connected') {
          // hack reloading page after login
          // location.reload();
          this.router.navigate(['list']);
          console.log('Logged In');
          console.log(res);
          return res;
        }else {
          this.router.navigate(['']);
          console.log('error');
          return res;
        }
      })
      .catch(this.handleError);

  }

  getStatus(): any {
    this.loginStatus = this.fb.getLoginStatus();
    return this.loginStatus;
  }
  getFriends() {
    this.fb.api('me?fields=friends')
      .then((res: any) => {
        console.log('Got the users friends', res);
      })
      .catch(this.handleError);
  }
  signOut(): any {
    this.fb.getLoginStatus().then((res: any) => {
      if (res.status !== 'conected') {
        this.fb.logout();
        this.router.navigate(['']);
        console.log('signed out');
        return res;
      }else {
        console.log('smtng goes wrong');
      }
    });
  }

  private handleError(error) {
    console.error('Error processing action', error);
  }
}
