// import { Injectable } from '@angular/core';
// import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
// import {Observable} from "rxjs";
// import {ActivatedRoute, Router, UrlTree} from "@angular/router";
//
//
// export interface Shoes{
//   shoes: {
//     shoesId?: string
//     shoesCode: number
//     shoesColor: string
//     shoesSeason: string
//     shoesType: string
//     shoesPicture: string
//     shoesShortDescription: string
//     shoesFullDescription: string
//   }
// }
//
// @Injectable()
// export class ClothesService{
//
//  shoes: Shoes ;
//   constructor(private http: HttpClient, private router: ActivatedRoute) {
//
//   }
//   private apiURL = 'http://localhost:8080/shoes';
//   private apiURLID = 'http://localhost:8080/shoes';
//   private apiUpdate = 'http://localhost:8080/shoes';
//   private apiDelete = 'http://localhost:8080/shoes/delete';
//   getClothes(): Observable<Object>{
//     return this.http.get(this.apiURL, {
//       observe: 'body',
//       responseType: 'json'
//     })
//   }
//   getById(query: string): Observable<any> {
//     console.log(query, 'service');
//     return this.http.get(this.apiURLID, {
//       params: new HttpParams().set('id', query),
//       observe: 'body',
//       responseType: 'json'
//     })
//
//   }
//   updateClothe(shoes):Observable<any>{
//     this.shoes = shoes;
//     console.log(this.shoes, 'Service')
//     return this.http.post(this.apiUpdate, shoes,{
//       observe: "events",
//       headers: new HttpHeaders({'Content-Type': 'application/json'}),
//       responseType: 'json'
//     });
//   }
//   deleteClothe(query: string): Observable<any>{
//     return this.http.delete(this.apiDelete,{
//       params: new HttpParams().set('id', query)
//     })
//   }
//   addClothe(shoes):Observable<any>{
//     this.shoes = shoes;
//    return this.http.post(this.apiURL, shoes, {
//      headers: new HttpHeaders({'Content-Type': 'application/json'})
//    })
//   }
//   }
