import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {Http, Response} from '@angular/http';
import {FacebookSocialModule} from '../Service/facebook-social.module';
import {LoginStatus} from 'ngx-facebook';
import {Router} from '@angular/router';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface Shoes{
  shoesId: string
  shoesCode: string
  shoesColor: string
  shoesSeason: string
  shoesType: string
  shoesPicture: string
  shoesShortDescription: string
  shoesFullDescription: string
}

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {

  private apiURL = 'http://localhost:8080/shoes/getAll';
  data: any = [{}];
  item: any = [{}];
  shoesId: string;
  itemId: string;
  constructor(private fb: FacebookSocialModule, private router: Router) {
/*    this.getCode();
    console.log(this.data);*/

  }
 /* getCode(): string {
    this.clothes.getClothes().subscribe(data => {
      console.log(data);
     this.data = data;
    });
    return this.data;
  }
  getById(shoesId){
    this.clothes.getById(shoesId).subscribe(itemData => {
      console.log(itemData);
      this.item = itemData;
    })
  }

  getStatus(): any  {
    this.fb.getStatus()
      .then(
        (res: LoginStatus) => {
          if (res.status === 'connected') {
            console.log(res.status);
            //this.router.navigate(['list']);
          } else {
            console.log('you are not connected');
            this.router.navigate(['']);
            console.log(res.status);
          }
        }
      );
  }*/
  ngOnInit() {
    jQuery(function($) {
      $('#list').on('click', function(event) {event.preventDefault();
      $('#products').find('.item')
        .addClass('list-group-item').addClass('list-item-pad'); });
      $('#grid').on('click', function(event) {event.preventDefault();
      $('#products').find('.item')
        .removeClass('list-group-item').removeClass('list-item-pad')
        .addClass('grid-group-item'); });
    });

  }

}
