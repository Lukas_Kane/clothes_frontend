///<reference path="../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import { Component, OnInit } from '@angular/core';
import {FacebookSocialModule} from '../Service/facebook-social.module';
import {Router} from '@angular/router';
import {LoginResponse, LoginStatus} from 'ngx-facebook';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  active: boolean;

  constructor(private fb: FacebookSocialModule, private router: Router) { }
  logout() {
    this.fb.signOut();
    this.fb.getStatus();
  }

  ngOnInit() {
    this.fb.getStatus()
      .then(
        (res: LoginStatus) => {
          if (res.status === 'connected') {
            this.active = true;
            console.log(res.status, 'header');
            // this.router.navigate(['list']);
          } else {
            this.router.navigate(['']);
            this.active = false;
            console.log('you are not connected');
            console.log(res.status);
          }
        }
      );
  }

}
