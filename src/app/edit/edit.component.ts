/*
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';?
import {HttpEvent, HttpEventType} from "@angular/common/http";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  data: any = {};
  id: string;
  item: any = {};
  constructor(private clothesService: ClothesService, private queryParam: ActivatedRoute) {
    this.getQueryParams();
    console.log(this.id);
    this.getById(this.id);
    console.log(this.data);
  }

  getQueryParams(): string{
    this.queryParam.queryParams.subscribe((params: Params) => {
      this.id = params['id']
    });
    return this.id
  }
  getById(id){
    this.clothesService.getById(id).subscribe((data) => {
      console.log(data);
      this.data = data;
    })
  }
  update(data){
    this.getQueryParams()
    this.clothesService.updateClothe(this.data).subscribe((event: HttpEvent<any>) => {

      if (event.type === HttpEventType.Response)
          console.log(event.body, 'Edit')
    })
  }
  ngOnInit() {
  }

}
*/
